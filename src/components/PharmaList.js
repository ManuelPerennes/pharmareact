import React from 'react';
import Pharmacie from './Pharmacie.js';
import axios from 'axios';
class PharmaList extends React.Component {

    constructor(props) {
        super(props);
        this.state = { pharmacies: [] };
    }

    componentDidMount() {
        axios.get('http://127.0.0.1:8000/pharma')
            .then(pharmapi => {
                console.log(pharmapi.data);
                this.setState({ pharmacies: pharmapi.data });
            });
    }
    render() {
        return (
            <ul>
                {this.state.pharmacies
                    .map(pharmacie => (
                        <Pharmacie key={pharmacie.id} fiche={pharmacie} />
                    ))}
            </ul>
        )
    }
}

export default PharmaList;