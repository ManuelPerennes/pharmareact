
import DeleteButton from './DeleteButton.js'
function Pharmacie(props) {
    return (
        <li key={props.fiche.id}>
            <div>
                <strong>{props.fiche.nom}</strong>
                <br />
                {props.fiche.quartier} /&nbsp;
                {props.fiche.ville}
            </div>
            <div>{props.fiche.garde}</div>
            <DeleteButton idKey={props.fiche.id}/>
            {/* <button onClick={modiPharma}>Modifier pharmacie</button> */}
        </li >
    );
}
export default Pharmacie;