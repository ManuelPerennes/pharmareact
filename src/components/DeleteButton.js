import axios from 'axios';
import React from 'react';

class DeleteButton extends React.Component {
   

    deletePharma () {
        console.log(this.props.idKey)
        
        axios.delete(`http://127.0.0.1:8000/pharma/${this.props.idKey}`)
        .then(() => alert('La pharmacie '+ this.props.idKey +' a été supprimée avec succès'))
        .catch(e => console.log(e));
        
    }

    render(){
        return (
            <button onClick={()=>this.deletePharma()}>Supprimer</button>
        )
    }
}
export default DeleteButton;