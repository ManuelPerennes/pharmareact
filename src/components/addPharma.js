import React from 'react';
import axios from 'axios';
class addPharma extends React.Component {
    constructor(props) {
        super(props);   
        this.state = {   
          nom : "",
          quartier : "",
          ville : "",
          garde : "", };
          this.handleSubmit = this.handleSubmit.bind(this);
          this.handleInputChange = this.handleInputChange.bind(this);
        }
      
  handleInputChange(event) {
    console.log("handleInputChange à l'appareil, j'écoute "+ event.target.value)
    console.log("HELP, que veut dire event.target.name ? "+ event.target.name)
    this.setState({
      [event.target.name] :event.target.value
    });
  }
  
  handleSubmit(event){
    alert('pharmacie enregistrée' )
    event.preventDefault();
        axios.post('http://127.0.0.1:8000/pharma', {
                nom : this.state.nom,
                quartier : this.state.quartier,
                ville : this.state.ville,
                garde : this.state.garde,}           
        )
        this.setState(
          {   
            nom : "",
            quartier : "",
            ville : "",
            garde : "", }
        )}
  
        render() {
          return (
            <form onSubmit={this.handleSubmit}>
                <label>
                nom :
                <input type="text" value={this.state.nom} onChange={this.handleInputChange} name= 'nom' />        </label>
                <label>
                ville :
                <input type="text" value={this.state.ville} onChange={this.handleInputChange} name= 'ville' />        </label>
                
                <label>
                quartier :
                <input type="text" value={this.state.quartier} onChange={this.handleInputChange} name= 'quartier' />        </label>
                <label>
                garde
                <input type="text" value={this.state.garde} onChange={this.handleInputChange} name= 'garde' />        </label>
              <input type="submit" value="Envoyer" />
            </form>
          );
        }
    }
export default addPharma;
