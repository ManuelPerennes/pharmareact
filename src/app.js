// App.js

import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import addPharma from './components/addPharma';

import Garde from './components/Garde';
import Clock from './components/Horaires';
import PharmaList from './components/PharmaList';



class App extends Component {
  render() {
    return (
    <Router>


        <div>
        <Clock/>
          <h2>Welcome to React Router Tutorial</h2>
          
          
         
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
              
          <ul className="navbar-nav mr-auto">
    
            <li><Link to={'/PharmaList'} className="nav-link"> Les pharmacies </Link></li>
            <li><Link to={'/Garde'} className="nav-link">Pharmacies de garde</Link></li>
            <li><Link to={'/addPharma'} className="nav-link">Ajouter une pharmacie</Link></li>
            
          </ul>
          </nav>
         
          <Switch>
              <Route exact path='/PharmaList' component={PharmaList} />
              <Route path='/Garde' component={Garde} />
              <Route path='/addPharma' component={addPharma} />
           
          </Switch>
        </div>
      </Router>
    );
  }
}

// setInterval(Horloge, 1000);

export default App;